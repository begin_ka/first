<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="css/admin.css">

    <title>Панель администратора сайта</title>

</head>
<body>    
    <div class='table'>
        <div class='table-wrapper'>            
            <div class='table-title'>Войти в <br>личный кабинет</div>
            <div class='table-content'>
                <form action="login.php" method='post' id='login-form' class='login-form'>
                    <input type='text' placeholder='Логин' class='input'name='login' value="" required><br>
                    <input type='password' placeholder='Пароль' class='input'name='password' value="" required><br>
                    <input type="email" placeholder="*E-mail" class="input" name="email" value="" required> <br>
                    <input type='submit' value='Войти' class='button'>
                </form>
            </div>
        </div>
    </div>

    <!--Form inter-->
    <script src="./js/app.js"></script>
</body>
</html>